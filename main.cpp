#include <fstream>
#include <iomanip>
#include <iostream>
#include <thread>
#include <mutex>
#include <string>
#include <map>

#include "pugixml/pugixml.hpp"

using namespace std;


////////////////////////////////////////////////
struct _Data{
    map<string,string> mPairs; //msisdn->balance
    mutex mMutex;
    bool mReady;

    _Data():mReady(false) {}
};

_Data _mem;

void processXML() {
    try{
        while(true){
            pugi::xml_document doc;
            pugi::xml_parse_result result = doc.load_file("input.xml");
            if (!result){
                cout << "Can`t open file " << result << endl;
                cout << "Wait for xml." << endl;
                this_thread::sleep_for(chrono::milliseconds(1000));
            }else{
                lock_guard<mutex> loguard(_mem.mMutex);
                for (pugi::xml_node tool: doc.child("subscribers"))
                {
                    string balance = tool.child("balance").child_value();
                    string attr = tool.attribute("msisdn").as_string();
                    _mem.mPairs[attr]=balance;
                }
                _mem.mReady=true;
                cout << "processXML done." << endl;
                break;
            }
        }
    }catch(...) {
        cout << "Unknown exc '" << __func__ << "'" << endl;
    }
}

void processJSON() {
    try{
        while(true){
            unique_lock<std::mutex> lguard(_mem.mMutex);
            if(!_mem.mReady) {
                lguard.unlock();
                cout << "Wait for data." << endl;
                this_thread::sleep_for(chrono::milliseconds(500));
            }else{
                string fname("output.json");
                ofstream fOut(fname);
                bool first=true;
                fOut << "{\n    \"subscribers\": [";
                for(const auto& kv:_mem.mPairs) {
                    if(first) {
                        first = false;
                        fOut << "\n";
                        fOut << setw(9) << "{" << endl;
                        fOut << "            " << "\"msisdn\": \"" << kv.first << "\"," << endl;
                        fOut << "            " << "\"balance\": " << kv.second << endl;
                        fOut << setw(9) << "}";
                    }else{
                        fOut <<  ",\n";
                        fOut << setw(9) << "{" << endl;
                        fOut << "            " << "\"msisdn\": \"" << kv.first << "\"," << endl;
                        fOut << "            " << "\"balance\": " << kv.second << endl;
                        fOut << setw(9) << "}";
                    }
                }
                if(_mem.mPairs.empty()) fOut<< "]";
                else fOut << "\n" << setw(5) << "]";
                fOut << "\n}" << endl;
                cout << "Result saved to " << fname << endl;
                break;
            }
        }
    }catch(...) {
        cout << "Unknown exc '" << __func__ << "'" << endl;
    }
}

int main()
{
    cout << "---" << endl;
    std::thread thXML(processXML);
    std::thread thJSON(processJSON);

    thXML.join();
    thJSON.join();
    return 0;
}
